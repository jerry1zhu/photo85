package model;

import java.io.Serializable;

/**
 * Association Class Created when one User shares an Album with another User
 * @author Jerry Zhu
 * @author Chris Hartley
 */
public class SharedAlbum implements Serializable{
	public Album shared;
	public String owner;
	public String sharedWith;
	
	public SharedAlbum(Album a, User o, String s) {
		
		shared = a;
		owner = o.username;
		sharedWith = s;
	}
	
	/**
	 * Shares this album with share
	 * @param share (username of user to share with)
	 * @return true if shared succesfully, false otherwise
	 */
	public Boolean shareWith(String share) {
		
		User target = User.getUserFromFile(share);
		if (target == null) return false;
		
		target.sharedAlbums.add(this);
		target.saveSelf();
		return true;
	}
	
	public String toString() {
		return shared.toString();
	}
	
	
	
}
