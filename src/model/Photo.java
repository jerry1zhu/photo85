package model;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;


public class Photo implements Serializable{
	public String name;
	public String path;
	public String caption = "";
	public Calendar date = Calendar.getInstance();
	public ArrayList<Tag> tags = new ArrayList<Tag>();
	
	public Photo(String name, String path) {
		this.name = name;
		this.path = path;
		
		File photo = new File(path);
		
		//converts photo.lastModified from milliseconds to YYYYMMDD
		date.setTimeInMillis(photo.lastModified());
		date.set(Calendar.MILLISECOND, 0);
	}
	
	/**
	 * adds tag to this photos list of tags
	 * @param tag
	 */
	public void addTag(Tag tag) {
		tags.add(tag);
	}
	
	public String toString() {
		return this.name;
		
	}
	
	
}
