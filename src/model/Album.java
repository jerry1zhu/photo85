package model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


public class Album implements Serializable{
	
	public String name = "";
	public ArrayList<Photo> photos = new ArrayList<Photo>();
	public Calendar beginDate;
	public Calendar endDate;
	
	
	public Album(String name, ArrayList<Photo> photos ) {
		this.name = name;
		for(Photo p : photos) {
			addPhoto(p);
		}
		
	}
	
	/**
	 * Set name of album
	 * @param newName
	 */
	public void setName(String newName) {
		name = newName;
	}
	
	/**
	 * Add photo to album and update Album date range
	 * @param photo
	 */
	public void addPhoto(Photo photo) {
		photos.add(photo);
		setDateRange();
		
	}
	
	/**
	 * Remove photo from album and update Album date range
	 * @param photo
	 */
	public void deletePhoto(Photo photo) {
		photos.remove(photo);
		setDateRange();
	}
	
	/**
	 * Remove photo at index i from album and update Album date range
	 * @param i
	 */
	public void deletePhoto(int i) {
		photos.remove(i);
		setDateRange();
	}
	
	/**
	 * Goes through every photo  in album and finds set beginDate to the earliest date and endDate to the latest date
	 */
	public void setDateRange() {
		beginDate = null;
		endDate = null;
		
		if (photos.size() == 0) return;
		for(Photo p : photos) {
			checkDates(p);
		}
		
	}
	
	/**
	 * If photo was taken before beginDate, set beginDate to photo date.
	 * If photo was taken after endDate, set endDate to photo date.
	 * @param photo
	 */
	public void checkDates(Photo photo){
		if(beginDate == null || photo.date.before(beginDate)) beginDate = photo.date;
		if(endDate == null || photo.date.after(endDate)) endDate = photo.date;
	}
	
	
	public String toString() {
		
		int nameLength = name.length();
		String tName = name;
		if(nameLength > 20) {
			tName = name.substring(0, 20) + "...";
		}
		
		else if(nameLength < 3) {
			tName += "\t\t\t\t\t";// +tName;
		}
		
		else if(nameLength < 7) {
			tName += "\t\t\t\t";// +tName;
			
		}
		else if(nameLength < 12) {
			tName += "\t\t\t";// +tName;
			
		}
		else if(nameLength < 16) {
			tName += "\t\t";// +tName;
			
		}
		else if(nameLength < 20) {
			tName += "\t";// +tName;
			
		}
		
		String tSize = "" + photos.size();
		if(tSize.length() < 5) {
			for(int i = 0; i <= 5 - tSize.length(); i++) {
				tSize += " ";
			}
		}
		
		
		String bDate = "";
		String eDate = "";
		SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd");
		
		if (beginDate != null) {
			bDate = form.format(beginDate.getTime()); 
		}
		
		if (endDate != null) {
			eDate = form.format(endDate.getTime()); 
		}
		
		String spacer = "\t";
		return "Name: " + tName + spacer + "Picture Count: " + tSize + spacer + spacer +
				"From: " + bDate + spacer + spacer + "To: " + eDate;
 				
	}
	
	
}
