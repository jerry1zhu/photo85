package model;

import java.io.Serializable;

public class Tag implements Serializable{
	public String name = "";
	public String val = "";
	
	public Tag(String name, String val) {
		this.name = name;
		this.val = val;
	}
	
	public String toString() {
		return name + " = " + val; 
	}
}
