package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable{
	
	public static final String storageDirectory = "Users";
	
	public String username;
	public String name;
	public ArrayList<Album> albums = new ArrayList<Album>();
	public ArrayList<SharedAlbum> sharedAlbums = new ArrayList<SharedAlbum>();
	public ArrayList<String> tagHistory = new ArrayList<String>();
	
	public User(String username,String name) {
		this.username = username;
		this.name = name;
		tagHistory.add("location");
		tagHistory.add("person");
	}
	
	/**
	 * Creates an album named title which contains photos and then adds it to this
	 * @param title album name
	 * @param photos photos that will be add to the created album
	 *
	 */
	public void createAlbum(String title, ArrayList<Photo> photos) {
		albums.add(new Album(title, photos));
		saveSelf();
		
	}
	
	
	/**
	 * Removes album at index i from this users list of albums
	 * @param i (int index to remove)
	 */
	public void deleteAlbum(int i) {
		albums.remove(i);
		saveSelf();
	}
	
	/**
	 * Shares toShare with User target
	 * @param toShare (Album being shared)
	 * @param target username of User being shared with
	 * @return true if shared succesfully, false otherwise
	 */
	public Boolean shareAlbum(Album toShare, String target) {
		SharedAlbum shared = new SharedAlbum(toShare, this, target);
		
		//If able to share 
		if(shared.shareWith(target)) {
			
			//add album to shared albums and save
			sharedAlbums.add(shared);
			saveSelf();
			return true;
		}
		
		//User didn't exist
		return false;
	}
	
	/**
	 * Unhares album at index i from this users list of shared albums
	 * @param i (int index to remove)
	 * @return true if unshared succesfully, false otherwise
	 */
	public Boolean unshare(int i) {
		SharedAlbum unshare = sharedAlbums.get(i);
		User target;
		if(username.equals(unshare.owner)) {
			target = User.getUserFromFile(unshare.sharedWith);
		} else {
			target = User.getUserFromFile(unshare.owner);
		}
		
		if(target != null) {
			for(SharedAlbum s : target.sharedAlbums) {
				if (s.shared.name.equals(unshare.shared.name)){
					target.sharedAlbums.remove(s);
					break;
				}
			}
			target.saveSelf();
		}
		
		for(SharedAlbum s : sharedAlbums) {
			if (s.shared.name.equals(unshare.shared.name)){
				
				sharedAlbums.remove(s);
				break;
			}
		}
		saveSelf();
		return false;
	}
	
	/**
	 * gets album at index i of albums list to sets its name to newName
	 * @param i index of album to rename
	 * @param newName new name of album
	 */	
	public void renameAlbum(int i, String newName) {
		albums.get(i).setName(newName);
		saveSelf();
		
	}
	
	/**
	 * Serializes this User to Users/UserObjects/username.ser
	 */
	public void saveSelf() {
		try {
			FileOutputStream fos = new FileOutputStream(storageDirectory+File.separator+"UserObjects"+File.separator+username+".ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(this);
			oos.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Take in an observableList and store it into the userData.ser file in the Users
	 * folder.
	 * @param username
	 * @return Deserialized User object
	 */
	public static User getUserFromFile(String username){
		User getThisUser = null;
		
		try {
			File file = new File(storageDirectory+File.separator+"UserObjects"+File.separator+username+".ser");
			file.createNewFile();
			FileInputStream fis = new FileInputStream(storageDirectory+File.separator+"UserObjects"+File.separator+username+".ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			getThisUser = (User) ois.readObject();
			ois.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			
		}
		
		return getThisUser;
	}
	
	@Override
    public String toString() {
        return ("Username: "+this.username+" (Name: "+this.name+")");
    }

}
