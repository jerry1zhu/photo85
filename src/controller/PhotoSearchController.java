package controller;

import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.Tag;
import model.User;
/**
 * Controller for searching photos window
 * @author Jerry Zhu
 * @author Chris Hartley
 */
public class PhotoSearchController implements Initializable{
	public User user;

	@FXML DatePicker startDate;
	@FXML DatePicker endDate;
	@FXML TextField Tag1;
	@FXML TextField Tag2;
	@FXML ChoiceBox TagRelation;
	@FXML TextField Val1;
	@FXML TextField Val2;
	@FXML HBox Pair1;
	@FXML HBox Pair2;
	@FXML HBox PairRelation;
	
	int numTags;
	
	/**
	 * initalize method for the search window
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		setNoTags();
		
		startDate.setValue(LocalDate.parse("1970-01-01"));
		endDate.setValue(LocalDate.now());
	}
	
	
	/**
	 * Sets this controller's user
	 * @param user
	 */
	public void setUser(User user) {
		this.user = user;
	}

	
	/**
	 * Hides both tag fields
	 */
	public void setNoTags() {
		Pair1.setVisible(false);

		Pair2.setVisible(false);

		PairRelation.setVisible(false);
		
		numTags = 0;
	}

	/**
	 * Displays Tag1 and hides Tag2 and TagRelation 
	 */
	public void setOneTag() {

		Pair1.setVisible(true);

		Pair2.setVisible(false);

		PairRelation.setVisible(false);
		
		numTags = 1;
	}

	/**
	 * Displays Tag1, Tag2, and TagRelation 
	 */
	public void setTwoTags() {

		Pair1.setVisible(true);

		Pair2.setVisible(true);

		PairRelation.setVisible(true);
		
		numTags = 2;

	}
	
	/**
	 * Confirms the user wants to cancel and sets UserHome as the active scene
	 */
	public void cancelSearch() {
		Parent root;
		try {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Cancel Album");
			String s = "Are you sure you want to cancel this search?";
			alert.setContentText(s);
			ButtonType yes = new ButtonType("Yes");
	        ButtonType no = new ButtonType("No");	        
	        alert.getButtonTypes().clear();
	        alert.getButtonTypes().addAll(yes, no);
			Optional<ButtonType> result = alert.showAndWait();
			 
			if ((result.isPresent()) && (result.get() == yes)) {
				goHome();
			}
			
		} catch(Exception e) {
			
		}
	}
	
	/**
	 * Gets either the start or end date of the search
	 * @param which decides if start or end date will be selected
	 * @return Calendar with date representing the requested date
	 */
	public Calendar getDate(String which) {
		LocalDate localDate;
		
		if (which.equals("start")) localDate = startDate.getValue();
		else localDate = endDate.getValue();
		
		Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
		Date millidate = Date.from(instant);
		Calendar date = Calendar.getInstance();
		date.setTime(millidate);
		
		if (which.equals("start")) {
			date.set(Calendar.HOUR, 0);
			date.set(Calendar.MINUTE, 0);
			date.set(Calendar.SECOND, 0);
			date.set(Calendar.MILLISECOND, 0);
		}
		else {
			date.set(Calendar.HOUR, 24);
			date.set(Calendar.MINUTE, 59);
			date.set(Calendar.SECOND, 59);
			date.set(Calendar.MILLISECOND, 0);
		}
		
		return date;
	}
	
	/**
	 * Searches all albums from this.user for Photos matching the search criteria. Does not include duplicates
	 */
	public void search() {
		ArrayList<Photo> results = new ArrayList<Photo>();
		
		for(Album a: user.albums) {
			for(Photo p : a.photos) {
				if(inDateRange(p) && hasRequiredTags(p) && !inList(results, p)) results.add(p);
			}
		}
		
		loadResults(results);
		
	}
	
	/**
	 * Check if p is already in results
	 * @param results
	 * @param p
	 * @return true if p is in results, false otherwise
	 */
	public Boolean inList(ArrayList<Photo> results, Photo p) {
		if (results.size() == 0 ) return false;
		for (Photo test: results ) {
			if(test.path.equals(p.path)) return true;
		}
		return false;
	}
	
	/**
	 * Checks if p was created within this search's date range
	 * @param p
	 * @return true if p was created in date range, false otherwise 
	 */
	public Boolean inDateRange(Photo p) {
		Calendar start = getDate("start");
		Calendar end = getDate("end");
		
		return (p.date.compareTo(start) >=0 && p.date.compareTo(end) <= 0 );
	}
	
	
	/**
	 * Checks if p has the tags required by this search
	 * @param p
	 * @return true if p has necessary tags, false otherwise 
	 */
	public Boolean hasRequiredTags(Photo p) {
		
		if(numTags == 1) {
			for(Tag t: p.tags) {
				
				//If the key is the same and the value is the same
				if(t.name.equals(Tag1.getText().trim()) &&
						t.val.equals(Val1.getText().trim())) {
					return true;
				}
			}
			
			return false;
		}
		
		if(numTags == 2) {
			Boolean first = false;
			Boolean second = false;

			for(Tag t: p.tags) {
				
				//If the key is the same and the value is the same
				if(t.name.equals(Tag1.getText().trim()) &&
						t.val.equals(Val1.getText().trim())) {
					first = true;
				}
				
				if(t.name.equals(Tag2.getText().trim()) &&
						t.val.equals(Val2.getText().trim())) {
					second = true;
				}
			}
			
			if(TagRelation.getValue().equals("or")) {
				return first || second;
			}
			else return first && second;
			
		}
		
		return numTags == 0;
	}
	
	/**
	 * Loads results into SearchResultController and make SearchResult the active scene
	 * @param results
	 */
	public void loadResults(ArrayList<Photo> results) {
		Parent root;
		try {
			
			//Created FXMLLoader in order to load controller as an object
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/SearchResult.fxml"));
			
			//Load the file into root
			root = loader.load();
			
			//Get the controller as an object
			SearchResultController controller = loader.<SearchResultController>getController();
			
			//Call setUser() on UserHomeController
			controller.setUser(user);
			controller.setResults(results);
			controller.loadPhotos();
			
			Stage stage = new Stage();
			stage.setTitle("User Albums");
			stage.setScene(new Scene(root));
			stage.show();
			startDate.getScene().getWindow().hide();
			
		} catch(Exception e) {
			
		}
	}
	
	/**
	 * Sets UserHome as Active scene
	 */
	public void goHome() {
		try {
			
			//Created FXMLLoader in order to load controller as an object
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/UserHome.fxml"));
			
			//Load the file into root
			Parent root = loader.load();
			
			//Get the controller as an object
			UserHomeController controller = loader.<UserHomeController>getController();
			
			//Call setUser() on UserHomeController
			controller.setUser(user);
			controller.loadAlbums();
			
			Stage stage = new Stage();
			stage.setTitle("User Albums");
			stage.setScene(new Scene(root));
			stage.show();
			startDate.getScene().getWindow().hide();
			
		}catch(Exception e) {
			
		}
	}

}
