package controller;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

import model.*;
/** Controller for the main login page for user. Allows the user to login to valid username or login the admin account
 * @author Chris Hartley and Jerry Zhu
 */
public class Controller {

	//general
	@FXML Button closeForm;
	
	//login.fxml
	@FXML TextField usernameEntered;
	@FXML Button login;
	
	//adminHome.fxml
	@FXML Button logout;
	@FXML ListView<String> userListView = new ListView<String>();
	
	
	private static User currentUser;
	//serialization 
	public static final String storageDirectory = "Users";
	public static final String fileName = "listOfUsernames.ser";
	
	public static ObservableList<String> list = FXCollections.observableList(new ArrayList<>());
	
	
	/**
	 * Utility method to set the current user variable. Allows the username to saved
	 * throughout the stages
	 * @param user (String for currentUser to be set to)
	 */
	public static void setCurrentUser(User user) {
		currentUser = user;
	}
	
	/**
	 * Utility method to return the current user variable.
	 * @return (String currentUser)
	 */
	public static String getCurrentUsername() {
		return currentUser.username;
	}
	/**
	 * Handles the Login Button, checks if the user has entered the right username.
	 * If correct the user will be directed to the user or admin home page. If wrong
	 * the user will be prompted that the username is not correct and will be asked to
	 * input another username.
	 * @param event clicking on the Add user button
	 */
	public void handleLoginButton(ActionEvent event) {
		String username = usernameEntered.getText().trim();
		
		//Admin user
		if(username.equalsIgnoreCase("admin")) {
			Parent root;
			try {
				root = FXMLLoader.load(getClass().getResource("/view/adminHome.fxml"));
				Stage stage = new Stage();
				stage.setTitle("Admin Home Page");
				stage.setScene(new Scene(root,400,400));
				stage.show();
				login.getScene().getWindow().hide();
			} catch(Exception e) {
			}
		}
		
		//non-Admin user
		else {
			//need to check what in the list of users
			
			//if username is wrong
			
        	try {
        		list = readData();
        		boolean correctLogin = false;
        		for(String s: list) {
	            	if(s.equalsIgnoreCase((username))) {
	            		correctLogin = true;
	            		User u = User.getUserFromFile(username);
	            		currentUser = u;
	            		break;
	            	}
        		}
        		
        		if(correctLogin == true) {
        			Parent root;
        			try {
        				
        				//Created FXMLLoader in order to load controller as an object
        				FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/UserHome.fxml"));
        				
        				//Load the file into root
        				root = loader.load();
        				
        				//Get the controller as an object
        				UserHomeController controller = loader.<UserHomeController>getController();
        				
        				//Call setUser() on UserHomeController
        				controller.setUser(currentUser);
        				controller.setUserList(list);
        				controller.loadAlbums();
        				
        				Stage stage = new Stage();
        				stage.setTitle("User Albums");
        				stage.setScene(new Scene(root));
        				stage.show();
        				login.getScene().getWindow().hide();
        			} catch(Exception e) {

        				e.printStackTrace();
        			}

        		}
        		else {
        			Alert alert = new Alert(AlertType.INFORMATION);
                	alert.setTitle("Username not found");
                	alert.setHeaderText("Incorrect Username");
                	alert.setContentText("Please enter a valid username!");
                	alert.showAndWait();
        		}
        	}catch(Exception e) {
				
			}
		}
	}
	
	/**
	 * Take in an observableList and store it into the userData.ser file in the Users
	 * folder.
	 * @throws ClassNotFoundException
	 * @return List of usernames
	 */

	public static ObservableList<String> readData() throws ClassNotFoundException {
		try {
			FileInputStream fis = new FileInputStream(storageDirectory+File.separator+fileName);
			ObjectInputStream ois = new ObjectInputStream(fis);
			List<String> dataList = (List<String>) ois.readObject();
			ois.close();
			
			return FXCollections.observableList(dataList);
		}catch (FileNotFoundException e) {
	        
	    } catch (IOException e) {
	        
	    }
		return FXCollections.emptyObservableList();
	}
	

	
}
