package controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.stage.Modality;
import model.*;

/**
 * Controllers the information going for the admin page. This allows the admin to user to view/create/delete/users
 * 
 * @author Jerry Zhu
 * @author Chris Hartley
 *
 */
public class adminController implements Initializable{

	@FXML Button logout;
	@FXML ListView<User> userListView = new ListView<User>();
	@FXML Button closeForm;
	@FXML TextField addingUsername;
	@FXML TextField addingName;
	
	//serialization 
	public static final String storageDirectory = "Users";
	public static final String fileName = "listOfUsernames.ser";
	
	public static ObservableList<User> userList = FXCollections.observableList(new ArrayList<>());
	public static ObservableList<String> listOfUsernames = FXCollections.observableList(new ArrayList<>());
	
	/**
	 * Initalize the Admin Page
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		//listOfUsernamesSerialization(listOfUsernames);
		userList = FXCollections.observableList(new ArrayList<>());
		listOfUsernames = SerializedFileToStringList();
		createUserList(listOfUsernames);
		userListView.setItems(userList);
		//each user in userList has a toString() method
		//I made userListView have generic type <User>
		//The observable list auto-magically calls User.toString when displaying
	}
	
	/**
	 * called just to refresh the user page after admin adds a user
	 */
	public void refresh() {
		userList = FXCollections.observableList(new ArrayList<>());
		listOfUsernames = SerializedFileToStringList();
		createUserList(listOfUsernames);
		userListView.setItems(userList);
	}
	
	/**
	 * Opens a form where the admin can enter the new user information
	 * @param event (clicking the add user button on the admin page)
	 */
	public void handleAddUserButton(ActionEvent event) {
		Parent root;
		//userListView.setItems(userList);
		try {
			root = FXMLLoader.load(getClass().getResource("/view/addUserForm.fxml"));
			Stage stage = new Stage();
			stage.setTitle("Adding User");
			stage.setScene(new Scene(root,300,400));
			//stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
			//logout.getScene().getWindow().hide();
			stage.setOnHiding(new EventHandler<WindowEvent>() {
			      public void handle(WindowEvent we) {
			          refresh();
			      }
			  });
		} catch(Exception e) {
			
		}
		
	}
	/**
	 * Removes user from the admin list and removes the correct files from the Users folder
	 * @param event (when the user clicks the delete button)
	 */
	public void handleDeleteUserButton(ActionEvent event){
		if(userListView.getSelectionModel().getSelectedIndex() != -1) {
			deleteUserFile(userList.get(userListView.getSelectionModel().getSelectedIndex()).username);
			userList.remove(userListView.getSelectionModel().getSelectedIndex());
		}
		
	}
	/**
	 * Takes place in the adding user form this is will handle getting new login info from the user
	 * @param event (when the user clicks the add button)
	 */
	public void addingNewUser(ActionEvent event){
		boolean exists = false;
		String getUsername = addingUsername.getText().trim();
		String getName = addingName.getText().trim();
		
		if(getUsername.isEmpty() || getName.isEmpty()) {
			if(getUsername.isEmpty() && getName.isEmpty()) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Textfield Error");
				alert.setHeaderText("Username and Name TextField Error");
				alert.setContentText("Please enter  a username and name!");
				alert.showAndWait();
			}
			else if(getUsername.isEmpty() || getUsername.equalsIgnoreCase("admin")){
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Textfield Error");
				alert.setHeaderText("Username Error");
				alert.setContentText("Please enter  a valid!");
				alert.showAndWait();
			}
			else {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Textfield Error");
				alert.setHeaderText("Name TextField Error");
				alert.setContentText("Please enter  a valid name!");
				alert.showAndWait();
			}
		}
		else {
			for(String s: listOfUsernames) {
				if(s.equalsIgnoreCase(getUsername)) {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Textfield Error");
					alert.setHeaderText("Username TextField Error");
					alert.setContentText("Username Already exists");
					alert.showAndWait();
					exists = true;
					break;
				}
			}
			if(exists == false) {
				User newUser = new User(getUsername,getName);
				//userList.addAll(newUser);
				//this will add a string to the listOfUsernames
				listOfUsernames.addAll(newUser.username);
				listOfUsernamesSerialization(listOfUsernames);
				createNewUserFile(newUser);
				closeForm.getScene().getWindow().hide();
			}
			//userListView.getScene().getWindow().hide();
			/*
			Parent root;
			try {
				root = FXMLLoader.load(getClass().getResource("/view/adminHome.fxml"));
				Stage stage = new Stage();
				stage.setTitle("Admin Home Page");
				stage.setScene(new Scene(root,400,400));
				stage.show();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				
			}
			*/
			
			
		}
	}
	
	/**
	 * This will handle the logout button for the users
	 * @param event clicking on the logout button
	 */
	public void handleLogoutButton(ActionEvent event) {
		Parent root;
		try {
			/*logout confirmation alert*/
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Logging out");
			String s = "Are you sure you want to logout?";
			alert.setContentText(s);
			ButtonType yes = new ButtonType("Yes");
	        ButtonType no = new ButtonType("No");	        
	        alert.getButtonTypes().clear();
	        alert.getButtonTypes().addAll(yes, no);
			Optional<ButtonType> result = alert.showAndWait();
			 
			if ((result.isPresent()) && (result.get() == yes)) {
				root = FXMLLoader.load(getClass().getResource("/view/login.fxml"));
				Stage stage = new Stage();
				stage.setTitle("Photo Album by Chris Hartley and Jerry Zhu");
				stage.setScene(new Scene(root,400,400));
				stage.show();
				logout.getScene().getWindow().hide();
			}
			
		} catch(Exception e) {
		}
	}

	/**
	 * Take a list of strings that contains the usernames and will serialize them into the listOfUsernames.ser
	 * file.
	 * @param listOfUsername (Observable)
	 */
	public void listOfUsernamesSerialization(ObservableList<String> listOfUsername) {
		try {
			File file = new File(storageDirectory+File.separator+fileName);
			file.createNewFile();
			FileOutputStream fos = new FileOutputStream(storageDirectory+File.separator+fileName);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(new ArrayList<String>(listOfUsername));
			oos.close();
		}catch(IOException e){
		}
	}
	
	/**
	 * This will take in the serialized file listOfUsernames.ser and convert it into a List
	 * @return Will return the List
	 */
	public ObservableList<String> SerializedFileToStringList(){
		List<String> stringList = new ArrayList<String>();
		try {
			File file = new File(storageDirectory+File.separator+fileName);
			file.createNewFile();
			FileInputStream fis = new FileInputStream(storageDirectory+File.separator+fileName);
			ObjectInputStream ois = new ObjectInputStream(fis);
			stringList = (List<String>) ois.readObject();
			ois.close();
		} catch(IOException e) {
			//No userlist. Don't do anything
		} catch (ClassNotFoundException e) {
			//Hasn't been thrown, but need to be caught
		}
		return FXCollections.observableList(stringList);
	}
	
	
	/**
	 * This function will take a new User object and convert it into a file that can be used later
	 * @param newUser (new User object)
	 */
	public void createNewUserFile(User newUser) {
		try {
			//File is going to be called User/*username*.ser
			
			File file = new File(storageDirectory+File.separator+"UserObjects"+File.separator+newUser.username+".ser");
			file.createNewFile();
			FileOutputStream fos = new FileOutputStream(storageDirectory+File.separator+"UserObjects"+File.separator+newUser.username+".ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(newUser);
			oos.close();
		} catch(IOException e) {
		}
	}
	
	/**
	 * this function will delete the file from the Users directory. Takes in a username, in the form of a String, and deletes the file that has the name
	 * @param deleteUsername (String)
	 */
	public void deleteUserFile(String deleteUsername) {
		
		File file = new File(storageDirectory+File.separator+"UserObjects"+File.separator+deleteUsername+".ser");
		if(file.delete()) {
		}else {
			return;
		}
		
		listOfUsernames.remove(listOfUsernames.indexOf(deleteUsername));
		
		listOfUsernamesSerialization(listOfUsernames);
	}
	
	/**
	 * Go through the listOfUsernames and go through each of the User object and add it to the UserList
	 * which will be add the userListView
	 * @param listOfUsernames (ObservableList)
	 */
	public void createUserList(ObservableList<String> listOfUsernames) {
		
		
		try {
			for(String s:listOfUsernames) {
				File file = new File(storageDirectory+File.separator+"UserObjects"+File.separator+s+".ser");
				file.createNewFile();
				FileInputStream fis = new FileInputStream(storageDirectory+File.separator+"UserObjects"+File.separator+s+".ser");
				ObjectInputStream ois = new ObjectInputStream(fis);
				User addThisUser = (User) ois.readObject();
				ois.close();
				userList.addAll(addThisUser);
			}
		} catch(IOException e) {
			
		} catch(ClassNotFoundException e) {
		
		}
	}
}
