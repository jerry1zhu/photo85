package controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.Tag;
import model.User;
/**
 * Controller for creating an album
 * @author Jerry Zhu
 * @author Chris Hartley
 */
public class CreateAlbumController implements Initializable{
	
	
	public User owner;
	
	@FXML ListView<Photo> PhotoListView = new ListView<Photo>();
	
	@FXML TextField albumName;
	
	public static ObservableList<Photo> photoList = FXCollections.observableList(new ArrayList<>());
	
	/**
	 * Initialize the window
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		setPhotos(new ArrayList<Photo>());
		
	}
	
	/**
	 * Displays ArrayList of Photos in PhotoListView
	 * @param photos
	 */
	public void setPhotos(ArrayList<Photo> photos) {
		photoList = FXCollections.observableList(photos);
		PhotoListView.setItems(photoList);
	}
	
	
	/**
	 * Loads the stock photos in PhotoListView
	 */
	public void loadStockPhotos() {
		try {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Stock photos");
			String s = "Are you sure you want to remove the currently selected photos and load the stock photos?";
			alert.setContentText(s);
			ButtonType yes = new ButtonType("Yes");
	        ButtonType no = new ButtonType("No");	        
	        alert.getButtonTypes().clear();
	        alert.getButtonTypes().addAll(yes, no);
			Optional<ButtonType> result = alert.showAndWait();
			 
			if ((result.isPresent()) && (result.get() == no)) {
				return;
			}
			
		} catch(Exception e) {
			
		}
		Tag[] s1T = {new Tag("person", "rick"), new Tag("location", "smith home")};
		Photo s1 = new Photo("Stock Photo 1","Stock Photos/7SCp1VJ.jpg");
		s1.caption = "You Pass Butter";
		s1.tags.addAll(Arrays.asList(s1T));
		
		Tag[] s2T = {new Tag("person", "rick"), new Tag("person", "morty"), new Tag("location", "c-754")};
		Photo s2 = new Photo("Stock Photo 2","Stock Photos/rfgU2iR.jpg");
		s2.caption = "Infinite Realites";
		s2.tags.addAll(Arrays.asList(s2T));
		
		Tag[] s3T = {new Tag("person", "rick"), new Tag("person", "morty"), new Tag("location", "teeny-verse")};
		Photo s3 = new Photo("Stock Photo 3","Stock Photos/Rick Sanchez.jpg");
		s3.caption = "Peace Among Worlds";
		s3.tags.addAll(Arrays.asList(s3T));
		
		Tag[] s4T = {new Tag("person", "morty")};
		Photo s4 = new Photo("Stock Photo 4","Stock Photos/thumb-1920-863098.png");
		s4.caption = "Wanna see my Pog Collection?";
		s4.tags.addAll(Arrays.asList(s4T));
		
		Tag[] s5T = {new Tag("person", "mr. meeseeks"), new Tag("location", "smith home")};
		Photo s5 = new Photo("Stock Photo 5","Stock Photos/vGJyuKb.jpg");
		s5.caption = "I'm Mr. Meeseeks, look at me!";
		s5.tags.addAll(Arrays.asList(s5T));
		
		Photo[] photos = {s1, s2, s3, s4, s5};
		setPhotos(new ArrayList(Arrays.asList(photos)));
		albumName.setText("stock");
	}
	/**
	 * Gets file from FileChooser and adds it to the photoList
	 */
	public void addPhotos() {
		
		BasicFileAttributes attr; 
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Select Picture to Add");
		File photo = chooser.showOpenDialog(PhotoListView.getScene().getWindow());
		if(photo!=null) {
		try {
			attr = Files.readAttributes(photo.toPath(), BasicFileAttributes.class);
		} catch (IOException e) {
			
			return;
		}
		
		String name = "" + photo.getName();
		String path = "" + photo.toPath();

		photoList.add(new Photo(name,path));
		
		PhotoListView.setItems(photoList);
		}
		
	}
	

	/**
	 * Attempts to create Album with photos in photoListView
	 */
	public void createAlbum() {
		boolean repeat = false;
		String title = albumName.getText().trim();
		
		if(title.isEmpty()) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Textfield Error");
				alert.setHeaderText("Album Name Error");
				alert.setContentText("Please enter a name for this album!");
				alert.showAndWait();
		}
		
		else {
			
			for(Album a: owner.albums) {
				if(a.name.equals(title)) {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Textfield Error");
					alert.setHeaderText("Album Name Error");
					alert.setContentText("This album already exists!");
					alert.showAndWait();
					repeat = true;
					break;
				}
					
			}
			if(repeat == false) {
				ArrayList<Photo> photos = new ArrayList<Photo>();
				photos.addAll(photoList);
				owner.createAlbum(title,photos);
	 			goHome();
			}
		}
		
	}
	
	
	/**
	 * Set User object from the previous controller.
	 * @param user
	 */
	public void setOwner(User user) {
		this.owner = user;
	}
	
	/**
	 * Confirms the user wants to cancel and sets UserHome as the active scene
	 */
	public void cancelCreate() {
	
		try {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Cancel Album");
			String s = "Are you sure you want to cancel Album Creation?";
			alert.setContentText(s);
			ButtonType yes = new ButtonType("Yes");
	        ButtonType no = new ButtonType("No");	        
	        alert.getButtonTypes().clear();
	        alert.getButtonTypes().addAll(yes, no);
			Optional<ButtonType> result = alert.showAndWait();
			 
			if ((result.isPresent()) && (result.get() == yes)) {
				goHome();
			}
			
		} catch(Exception e) {
			
		}
		
	}
	
	/**
	 * Sets UserHome as the active scene
	 */
	public void goHome() {
		try {
			
			//Created FXMLLoader in order to load controller as an object
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/UserHome.fxml"));
			
			//Load the file into root
			Parent root = loader.load();
			
			//Get the controller as an object
			UserHomeController controller = loader.<UserHomeController>getController();
			
			//Call setUser() on UserHomeController
			controller.setUser(owner);
			controller.loadAlbums();
			Stage stage = new Stage();
			stage.setTitle("User Albums");
			stage.setScene(new Scene(root));
			stage.show();
			PhotoListView.getScene().getWindow().hide();
			
		}catch(Exception e) {
			
		}
	}
	
}
