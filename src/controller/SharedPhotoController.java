package controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.*;
/**
 * Controller that handles specific shared album window
 * @author Jerry Zhu
 * @author Chris Hartley
 */
public class SharedPhotoController implements Initializable{

	public static String owner;
	public static User user;
	public static Album album;
	public static int selection;
	
	@FXML Text albumNameTitle = new Text();
	@FXML Text albumPhotoNum = new Text();
	@FXML Text albumOwner = new Text();
	@FXML TextArea photoCaption = new TextArea();
	@FXML ListView<Photo> photoListView = new ListView<Photo>();
	@FXML ImageView imageSpace = new ImageView();
	@FXML ListView<Tag> tagListView = new ListView<Tag>();
	
	@FXML TextArea newCaption = new TextArea();
	
	@FXML TextField tagValue = new TextField();
	@FXML ComboBox tagType;
	
	@FXML Button closeAddTag = new Button();
	public static ObservableList<Tag> listOfTags= FXCollections.observableList(new ArrayList<>());
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		photoCaption.setEditable(false);
		
		if(tagType != null) {
			tagType.setItems(FXCollections.observableArrayList(user.tagHistory));
			tagType.setEditable(true);
		}
	}
	
	
	/**
	 * Set User object from previous stage
	 * @param user
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	/**
	 * Set username of album owner
	 * @param name
	 */
	public void setOwner(String name) {
		if(name.equals(user.username)) owner = "me";
		else owner = name;
	}
	
	
	/**
	 * Sets Album object from previous stage
	 * @param album
	 */
	public void setAlbum(Album album) {
		this.album = album;
	}
	
	/**
	 * Displays photos from this.album in the photoListView
	 */
	public void loadPhotos() {
		albumNameTitle.setText(album.name);
		albumPhotoNum.setText("Number of Photos:  "+album.photos.size());
		albumOwner.setText("Owned By:  "+owner);
		photoListView.setItems(FXCollections.emptyObservableList());
		photoListView.setItems(FXCollections.observableList(album.photos));
		
		if(album.photos.size() >  0) {
			photoListView.getSelectionModel().selectFirst();
			selection = 0;
			loadTags();
			selectPhoto();
		}
		
		else {
			photoCaption.setEditable(true);
			photoCaption.setText("");
			photoCaption.setEditable(false);
			imageSpace.setImage(null);
			listOfTags = FXCollections.observableList(new ArrayList<>());
			tagListView.setItems(listOfTags);
		}

	}
	
	
	/**
	 * Load the tags of currently selected photo into tagListView
	 */
	public void loadTags() {
		listOfTags = FXCollections.observableList(new ArrayList<>());
		tagListView.setItems(listOfTags);
		
		listOfTags = FXCollections.observableList(album.photos.get(selection).tags);
		tagListView.setItems(listOfTags);
		
	}
	
	/**
	 * Takes the currently selected photo and updates the display with all of the selected photo's information.
	 */
	public void selectPhoto() {
		
		int i = photoListView.getSelectionModel().getSelectedIndex();
		selection = i;
		File file = new File(album.photos.get(i).path);
		Image preview = new Image(file.toURI().toString());
		photoCaption.setEditable(true);
		photoCaption.setText(album.photos.get(i).caption);
		photoCaption.setEditable(false);
		imageSpace.setImage(preview);
		
		loadTags();
		
	}	
	
	/**
	 *  sets index of selected image to i and updates the display with all of the selected photo's information.
	 * @param i (selected image index)
	 */
	public void selectPhoto(int i) {
		photoListView.getSelectionModel().clearAndSelect(i);
		selectPhoto();
	
	}
	
	
	/**
	 * Sets CopyPhoto as active Scene
	 */
	public void copyPhoto() {
		Parent root;
		int i;
		try {
		
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CopyPhoto.fxml"));
		
			root = loader.load();
			
			CopyPhotoController controller = loader.<CopyPhotoController>getController();
			
			i = photoListView.getSelectionModel().getSelectedIndex();
			if(i == -1) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Photo Error");
				alert.setContentText("Please select a photo to copy");
				alert.showAndWait();
				return;
			}
				
			controller.setUser(user);
			controller.setAlbum(album);
			controller.loadAlbums();
			controller.setPhoto(album.photos.get(i));
		
			
			Stage stage = new Stage();
			stage.setTitle("Copy Photo");
			stage.setScene(new Scene(root));
			stage.show();
			photoListView.getScene().getWindow().hide();
		} catch(Exception e) {
			
		}
	}

		
	/**
	 * takes image that is selected and displays it in FullPicture, which becomes the active scene
	 * @param event (press Open Picture button)
	 */
	public void openPhoto(ActionEvent event) {
		
		int i = photoListView.getSelectionModel().getSelectedIndex();
		if(i != -1) {
			try {
				String path = album.photos.get(i).path;
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FullPicture.fxml"));
				Parent root = loader.load();
				openFullPhotoController controller = loader.<openFullPhotoController>getController();
				controller.setImage(path, album.photos.get(i).caption, album.photos.get(i).tags,album,i);
				controller.displayImage();
				Stage stage = new Stage();
				stage.setTitle(album.photos.get(i).name);
				stage.setScene(new Scene(root));
				stage.show();
				
			} catch(Exception e) {
				
			}
		}
	}
	
	/**
	 *  Load album photos in CreateAlbumController and sets CreateAlbum as active scene
	 * @param event
	 */
	public void copyAlbum(ActionEvent event) {
		Parent root;
		try {
			
			//Created FXMLLoader in order to load controller as an object
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CreateAlbum.fxml"));
			
			//Load the file into root
			root = loader.load();
			
			//Get the controller as an object
			CreateAlbumController controller = loader.<CreateAlbumController>getController();
			
			//Call setUser() on UserHomeController
			controller.setOwner(user);
			controller.setPhotos(album.photos);
			
			Stage stage = new Stage();
			stage.setTitle("Copy Results to Album");
			stage.setScene(new Scene(root));
			stage.show();
			photoListView.getScene().getWindow().hide();
			
		} catch(Exception e) {
			
		}
	}
	
	
	
	/**
	 * Sets UserHome as the active scene
	 * @param event (click the back button)
	 */
	public void backToUserHome(ActionEvent event) {
		try {
			
			//Created FXMLLoader in order to load controller as an object
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/SharedAlbums.fxml"));
			
			//Load the file into root
			Parent root = loader.load();
			
			//Get the controller as an object
			SharedAlbumsController controller = loader.<SharedAlbumsController>getController();
			
			//Call setUser() on UserHomeController
			controller.setUser(user);
			controller.loadAlbums();
			
			Stage stage = new Stage();
			stage.setTitle("User Albums");
			stage.setScene(new Scene(root));
			stage.show();
			photoListView.getScene().getWindow().hide();
			
		}catch(Exception e) {
			
		}
	}
	
}
