package controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.*;
/**
 * Controller that handles specific album window
 * @author Jerry Zhu
 * @author Chris Hartley
 */
public class PhotoController implements Initializable{

	public static User user;
	public static Album album;
	public static int selection;
	
	@FXML Text albumNameTitle = new Text();
	@FXML Text albumPhotoNum = new Text();
	@FXML TextArea photoCaption = new TextArea();
	@FXML ListView<Photo> photoListView = new ListView<Photo>();
	@FXML ImageView imageSpace = new ImageView();
	@FXML ListView<Tag> tagListView = new ListView<Tag>();
	
	@FXML TextArea newCaption = new TextArea();
	
	@FXML TextField tagValue = new TextField();
	@FXML ComboBox tagType;
	
	@FXML Button closeAddTag = new Button();
	public static ObservableList<Tag> listOfTags= FXCollections.observableList(new ArrayList<>());
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		photoCaption.setEditable(false);
		
		if(tagType != null) {
			tagType.setItems(FXCollections.observableArrayList(user.tagHistory));
			tagType.setEditable(true);
		}
	}
	
	
	/**
	 * Set User object from previous stage
	 * @param user
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	/**
	 * Sets Album object from previous stage
	 * @param album
	 */
	public void setAlbum(Album album) {
		this.album = album;
	}
	
	/**
	 * Displays photos from this.album in the photoListView
	 */
	public void loadPhotos() {
		albumNameTitle.setText(album.name);
		albumPhotoNum.setText("Number of Photos : "+album.photos.size());
		photoListView.setItems(FXCollections.emptyObservableList());
		photoListView.setItems(FXCollections.observableList(album.photos));
		
		if(album.photos.size() >  0) {
			photoListView.getSelectionModel().selectFirst();
			selection = 0;
			loadTags();
			selectPhoto();
		}
		
		else {
			photoCaption.setEditable(true);
			photoCaption.setText("");
			photoCaption.setEditable(false);
			imageSpace.setImage(null);
			listOfTags = FXCollections.observableList(new ArrayList<>());
			tagListView.setItems(listOfTags);
		}

	}
	
	
	/**
	 * Load the tags of currently selected photo into tagListView
	 */
	public void loadTags() {
		listOfTags = FXCollections.observableList(new ArrayList<>());
		tagListView.setItems(listOfTags);
		
		listOfTags = FXCollections.observableList(album.photos.get(selection).tags);
		tagListView.setItems(listOfTags);
		
	}
	
	/**
	 * Takes the currently selected photo and updates the display with all of the selected photo's information.
	 */
	public void selectPhoto() {
		
		int i = photoListView.getSelectionModel().getSelectedIndex();
		selection = i;
		File file = new File(album.photos.get(i).path);
		Image preview = new Image(file.toURI().toString());
		photoCaption.setEditable(true);
		photoCaption.setText(album.photos.get(i).caption);
		photoCaption.setEditable(false);
		imageSpace.setImage(preview);
		
		loadTags();
		
	}	
	
	/**
	 *  sets index of selected image to i and updates the display with all of the selected photo's information.
	 * @param i (selected image index)
	 */
	public void selectPhoto(int i) {
		photoListView.getSelectionModel().clearAndSelect(i);
		selectPhoto();
	
	}
	
	
	/**
	 * Sets CopyPhoto as active Scene
	 */
	public void copyPhoto() {
		Parent root;
		int i;
		try {
		
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CopyPhoto.fxml"));
		
			root = loader.load();
			
			CopyPhotoController controller = loader.<CopyPhotoController>getController();
			
			i = photoListView.getSelectionModel().getSelectedIndex();
			if(i == -1) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Photo Error");
				alert.setContentText("Please select a photo to copy");
				alert.showAndWait();
				return;
			}
				
			controller.setUser(user);
			controller.setAlbum(album);
			controller.loadAlbums();
			controller.setPhoto(album.photos.get(i));
		
			
			Stage stage = new Stage();
			stage.setTitle("Copy Photo");
			stage.setScene(new Scene(root));
			stage.show();
			photoListView.getScene().getWindow().hide();
		} catch(Exception e) {
			
		}
	}

	/**
	 * Sets MovePhoto as active Scene
	 */
	public void movePhoto() {
		Parent root;
		int i;
		try {
		
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/MovePhoto.fxml"));
		
			root = loader.load();
			
			CopyPhotoController controller = loader.<CopyPhotoController>getController();
			
			i = photoListView.getSelectionModel().getSelectedIndex();
			if(i == -1) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Photo Error");
				alert.setContentText("Please select a photo to copy");
				alert.showAndWait();
				return;
			}
				
			controller.setUser(user);
			controller.setAlbum(album);
			controller.loadAlbums();
			controller.setPhoto(album.photos.get(i));
		
			
			Stage stage = new Stage();
			stage.setTitle("Copy Photo");
			stage.setScene(new Scene(root));
			stage.show();
			photoListView.getScene().getWindow().hide();
		} catch(Exception e) {
			
		}
	}
		
	/**
	 * takes image that is selected and displays it in FullPictyre, which becomes the active scene
	 * @param event (press Open Picture button)
	 */
	public void openPhoto(ActionEvent event) {
		
		int i = photoListView.getSelectionModel().getSelectedIndex();
		if(i != -1) {
			try {
				String path = album.photos.get(i).path;
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/FullPicture.fxml"));
				Parent root = loader.load();
				openFullPhotoController controller = loader.<openFullPhotoController>getController();
				controller.setImage(path, album.photos.get(i).caption, album.photos.get(i).tags,album,i);
				controller.displayImage();
				Stage stage = new Stage();
				stage.setTitle(album.photos.get(i).name);
				stage.setScene(new Scene(root));
				stage.show();
				
			} catch(Exception e) {
				
			}
		}
	}
	
	
	/**
	 * Calls the form to edit the currently selected photo's caption
	 * @param event
	 */
	public void editCaption(ActionEvent event) {
		
		int i = photoListView.getSelectionModel().getSelectedIndex();
		selection = i;
		if(i != -1) {
			try {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/editCaption.fxml"));
				Parent root = loader.load();
				Stage stage = new Stage();
				stage.setTitle("Edit Caption");
				stage.setScene(new Scene(root));
				stage.show();
				stage.setOnHiding(new EventHandler<WindowEvent>() {
				      public void handle(WindowEvent we) {
				    	  int i = photoListView.getSelectionModel().getSelectedIndex();
				          loadPhotos();
				          selectPhoto(i);
				      }
				  });
				//photoListView.getScene().getWindow().hide();
			} catch(Exception e) {
				
			}
		}
	}
	
	/**
	 * Sets UserHome as the active scene
	 * @param event (click the back button)
	 */
	public void backToUserHome(ActionEvent event) {
		try {
			
			//Created FXMLLoader in order to load controller as an object
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/UserHome.fxml"));
			
			//Load the file into root
			Parent root = loader.load();
			
			//Get the controller as an object
			UserHomeController controller = loader.<UserHomeController>getController();
			
			//Call setUser() on UserHomeController
			controller.setUser(user);
			controller.loadAlbums();
			
			Stage stage = new Stage();
			stage.setTitle("User Albums");
			stage.setScene(new Scene(root));
			stage.show();
			photoListView.getScene().getWindow().hide();
			
		}catch(Exception e) {
			
		}
	}
	
	
	/**
	 * Takes information from the edit caption form and sets it to the currently selected photo. Then reloads the photos.
	 * @param event
	 */
	public void editingTheCaption(ActionEvent event) {
	
		String caption = newCaption.getText();
		album.photos.get(selection).caption = caption;
		user.saveSelf();
		loadPhotos();
		newCaption.getScene().getWindow().hide();
	}
	/**
	 * Call the new tag form
	 * @param event (add tag button)
	 */
	public void addTags(ActionEvent event) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/enterNewTag.fxml"));
			Parent root = loader.load();
			Stage stage = new Stage();
			stage.setTitle("Add a new tag");
			stage.setScene(new Scene(root));
			stage.show();
			stage.setOnHiding(new EventHandler<WindowEvent>() {
			      public void handle(WindowEvent we) {
			    	  int i = photoListView.getSelectionModel().getSelectedIndex();
			          loadPhotos();
			          selectPhoto(i);
			      }
			  });
		} catch (Exception e) {
			
		}
	}
	/**
	 * take information from the add tag form and added it to the photo then save to user
	 * @param event (click add tag in the add tag form)
	 */
	public void addingTags(ActionEvent event) {
		
		String value = tagValue.getText().trim();
		String type = tagType.getValue().toString().trim();
		
		Boolean inHistory = false;
		for(String tag : user.tagHistory) {
			if(tag.equals(type)) inHistory = true;
		}
		
		if(!inHistory) {
			user.tagHistory.add(type);
		}
		
		Photo photo = album.photos.get(selection);
			for(Tag t : photo.tags) {
				
				
				if (t.name.equals(type)) {
					
					if (t.name.equals("location")) {
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Tag Error");
						alert.setContentText("A photo may only have one \"location\" tag");
						alert.showAndWait();
						return;
					}
					
					if(t.val.equals(value)) {
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Tag Error");
						alert.setContentText("This photo already has tag: " + type + " = "+ value);
						alert.showAndWait();
						return;
					}
				}
			}
		
		photo.addTag(new Tag(type, value));
		user.saveSelf();
		loadTags();
		closeAddTag.getScene().getWindow().hide();
		
	}
	/**
	 * deletes currently selected tag
	 * @param event (delete tag button)
	 */
	public void deleteTags(ActionEvent event) {
		
		selection = photoListView.getSelectionModel().getSelectedIndex();
		//as long as there are tags in the hash table
		if(album.photos.get(selection).tags.size() > 0) {
			int i = tagListView.getSelectionModel().getSelectedIndex();
			if(i != -1) {
				album.photos.get(selection).tags.remove(i);
				loadTags();
				user.saveSelf();
			}
		}
	}
	/**
	 * Prompts user to add a new photo and if accepted adds the photo to the list
	 * @param event (add photo button)
	 */
	public void openNewPhoto(ActionEvent event) {
		BasicFileAttributes attr; 
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Select Picture to Add");
		File photo = chooser.showOpenDialog(photoListView.getScene().getWindow());
		if(photo!=null) {
		try {
			attr = Files.readAttributes(photo.toPath(), BasicFileAttributes.class);
		} catch (IOException e) {
			return;
		}
		
		String name = "" + photo.getName();
		String path = "" + photo.toPath();

		album.addPhoto(new Photo(name,path));
		user.saveSelf();
		loadPhotos();
		}
	}
	/**
	 * delete photo from the photo list
	 * @param event (delete photo button)
	 */
	public void deletePhoto(ActionEvent event) {
		album.deletePhoto(selection);
		
		user.saveSelf();
		loadPhotos();
	}
}
