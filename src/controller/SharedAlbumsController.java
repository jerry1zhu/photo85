package controller;

import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Album;
import model.SharedAlbum;
import model.User;


/**
 * Controller for the Shared Album page
 * @author Jerry Zhu
 * @author Chris Hartley
 */
public class SharedAlbumsController{


	public User user;

	@FXML 
	ListView<SharedAlbum> albumList = new ListView<SharedAlbum>();


	/**
	 * Set User object from previous stage
	 * @param user
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Loads shared albums from this.user into albumList
	 */
	public void loadAlbums() {

		//Changes only register if # of items changes so empty album list
		albumList.setItems(FXCollections.emptyObservableList());

		//...and then repopulate it to reflect changes
		albumList.setItems(FXCollections.observableList(user.sharedAlbums));

		//select first item
		if(user.albums.size() > 0) albumList.getSelectionModel().selectFirst();

	}
	
	
	/**
	 * Loads shared albums from this.user into albumList
	 */
	public void unshareAlbum() {
		int i = albumList.getSelectionModel().getSelectedIndex();
		user.unshare(i);
		loadAlbums();
	}

	/**
	 * Sets AlbumHome or SharedAlbumHome as the active scene 
	 * depending if this user owns the shared album
	 */
	public void openAlbum() {
		Parent root;
		int i = albumList.getSelectionModel().getSelectedIndex();
		if (user.sharedAlbums.get(i).owner.equals(user.username)) {
			openOwner();
		}else {
			openSharedWith();
		}
	}
	
	/**
	 * Sets SharedAlbumHome as the active scene ]
	 */
	public void openSharedWith() {
		Parent root;
		try {
			int i = albumList.getSelectionModel().getSelectedIndex();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/SharedAlbumHome.fxml"));

			root = loader.load();

			SharedPhotoController controller = loader.<SharedPhotoController>getController();

			controller.setUser(user);
			i = albumList.getSelectionModel().getSelectedIndex();
			if(i != -1) {
				controller.setAlbum(user.sharedAlbums.get(i).shared);
				controller.setOwner(user.sharedAlbums.get(i).owner);
				controller.loadPhotos();
				Stage stage = new Stage();
				stage.setTitle("Shared Album Photos");
				stage.setScene(new Scene(root));
				stage.show();
				albumList.getScene().getWindow().hide();
			}


		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * Sets AlbumHome as the active scene ]
	 */
	public void openOwner() {
		Parent root;
		try {
			int i = albumList.getSelectionModel().getSelectedIndex();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AlbumHome.fxml"));

			root = loader.load();

			PhotoController controller = loader.<PhotoController>getController();

			controller.setUser(user);
			i = albumList.getSelectionModel().getSelectedIndex();
			if(i != -1) {
				controller.setAlbum(user.sharedAlbums.get(i).shared);
				controller.loadPhotos();
				Stage stage = new Stage();
				stage.setTitle("Photos");
				stage.setScene(new Scene(root));
				stage.show();
				albumList.getScene().getWindow().hide();
			}


		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sets UserHome as the active scene
	 */
	public void goHome() {
		try {

			//Created FXMLLoader in order to load controller as an object
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/UserHome.fxml"));

			//Load the file into root
			Parent root = loader.load();

			//Get the controller as an object
			UserHomeController controller = loader.<UserHomeController>getController();

			//Call setUser() on UserHomeController
			controller.setUser(user);
			controller.loadAlbums();
			Stage stage = new Stage();
			stage.setTitle("User Albums");
			stage.setScene(new Scene(root));
			stage.show();
			albumList.getScene().getWindow().hide();

		}catch(Exception e) {

		}
	}




}
