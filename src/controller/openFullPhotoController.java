package controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import model.Album;
import model.Photo;
import model.Tag;
/**
 * Controller for full view of photo
 * @author Jerry Zhu
 * @author Chris Hartley
 */
public class openFullPhotoController {

	@FXML ImageView bigPicture = new ImageView();
	@FXML Text fullPictureCaption = new Text();
	@FXML Text fullPictureDate = new Text();
	@FXML Text listOfTags = new Text();
	public String path;
	public String caption;
	public ArrayList<Tag>  tags;
	public Album album;
	int selection;
	
	/**
	 * Setter method used to take photo from previous controller
	 * @param path (image path)
	 * @param caption (image caption)
	 * @param tags (image tags)
	 * @param album (image album)
	 * @param selection (image index)
	 */
	public void setImage(String path, String caption, ArrayList<Tag> tags,Album album,int selection) {
		this.path = path;
		this.caption = caption;
		this.tags = tags;
		this.album = album;
		this.selection = selection;
	}
	
	/**
	 * Creates image object and sets image and information on the window.
	 */
	public void displayImage() {
		String tagString = "" + tags;
		File file = new File(path);
		Image preview = new Image(file.toURI().toString());
		bigPicture.setImage(preview);
		fullPictureCaption.setText(caption);
		
		SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd   HH:mm:ss");
		fullPictureDate.setText(form.format(album.photos.get(selection).date.getTime()));
		
		listOfTags.setText(tagString);
	}
	/**
	 * Displays the next image in the list. 
	 * If the current image is the last image in the album then the first image will be displayed.
	 * @param event (clicking forward button)
	 */
	public void forwards(ActionEvent event) {
		
		if(selection == album.photos.size()-1)
			selection = 0;
		else
			selection+=1;
		Photo next = album.photos.get(selection);
		String tagString = ""+next.tags;
		File file = new File(next.path);
		Image preview = new Image(file.toURI().toString());
		bigPicture.setImage(preview);
		fullPictureCaption.setText(next.caption);
		SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd   HH:mm:ss");
		fullPictureDate.setText(form.format(album.photos.get(selection).date.getTime()));
		listOfTags.setText(tagString);
	}
	
	
	/**
	 * Displays the previous image in the list. 
	 * If the current image is the first image in the album then the last image will be displayed.
	 * @param event
	 */
	public void backwards(ActionEvent event) {
		if(selection -1 == -1)
			selection = album.photos.size() - 1;
		else
			selection-=1;
		Photo next = album.photos.get(selection);
		String tagString = ""+next.tags;
		File file = new File(next.path);
		Image preview = new Image(file.toURI().toString());
		bigPicture.setImage(preview);
		fullPictureCaption.setText(next.caption);
		SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd   HH:mm:ss");
		fullPictureDate.setText(form.format(album.photos.get(selection).date.getTime()));
		listOfTags.setText(tagString);
	}
	
	

	/**
	 * Hides the PhotoViewer which makes AlbumHome the active scene
	 */
	public void goHome() {
		bigPicture.getScene().getWindow().hide();
	}
}
