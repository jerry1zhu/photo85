package controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.*;
/**
 * Controller for the search result screen
 * @author Jerry Zhu
 * @author Chris Hartley
 */
public class SearchResultController implements Initializable{

	public static User user;
	public ArrayList<Photo> results;
	public static int selection;
	
	@FXML Text albumNameTitle;
	@FXML Text albumPhotoNum;
	@FXML TextArea photoCaption;
	@FXML ListView<Photo> photoListView;
	@FXML ImageView imageSpace;
	@FXML Button closeAddTag;
	@FXML ListView<Tag> tagListView;
	@FXML Label date;
	
	
	public static ObservableList<Tag> listOfTags= FXCollections.observableList(new ArrayList<>());
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		photoCaption.setEditable(false);
	}
	
	/**
	 * Set User object from previous stage
	 * @param user
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	/**
	 * Sets this controller's search results
	 * @param results
	 */
	public void setResults(ArrayList<Photo> results) {
		this.results = results;
	}
	
	/**
	 * Loads results into the photoListView
	 */
	
	public void loadPhotos() {
		albumNameTitle.setText("Search Results");
		albumPhotoNum.setText("Number of Photos : "+results.size());
		photoListView.setItems(FXCollections.emptyObservableList());
		photoListView.setItems(FXCollections.observableList(results));
		
		if(results.size() >  0) {
			photoListView.getSelectionModel().selectFirst();
			selection = 0;
			loadTags();
			selectPhoto();
		}
	}
	
	
	/**
	 * Load the tags for this photo into tagListView
	 */
	public void loadTags() {
		String entry;
		listOfTags = FXCollections.observableList(new ArrayList<>());
		tagListView.setItems(listOfTags);
		
		listOfTags = FXCollections.observableList(results.get(selection).tags);
		tagListView.setItems(listOfTags);
		
	}
	
	/**
	 * Takes the currently selected photo and updates the display with all of the selected photo's information.
	 */
	public void selectPhoto() {
		
		int i = photoListView.getSelectionModel().getSelectedIndex();
		selection = photoListView.getSelectionModel().getSelectedIndex();
		File file = new File(results.get(i).path);
		Image preview = new Image(file.toURI().toString());
		photoCaption.setEditable(true);
		photoCaption.setText(results.get(i).caption);
		photoCaption.setEditable(false);
		imageSpace.setImage(preview);
		SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd   HH:mm:ss");
		date.setText(form.format(results.get(i).date.getTime()));
		
		loadTags();
	}	
	
	/**
	 *  Load results in CreateAlbumController and sets CreateAlbum as active scene
	 * @param event
	 */
	public void copyToAlbum(ActionEvent event) {
		Parent root;
		try {
			
			//Created FXMLLoader in order to load controller as an object
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CreateAlbum.fxml"));
			
			//Load the file into root
			root = loader.load();
			
			//Get the controller as an object
			CreateAlbumController controller = loader.<CreateAlbumController>getController();
			
			//Call setUser() on UserHomeController
			controller.setOwner(user);
			controller.setPhotos(results);
			
			Stage stage = new Stage();
			stage.setTitle("Copy Results to Album");
			stage.setScene(new Scene(root));
			stage.show();
			photoListView.getScene().getWindow().hide();
			
		} catch(Exception e) {
			
		}
	}
	
	
	/**
	 *  Sets PhotoSearch as the active scene
	 */
	public void back() {
		
		try {
			
			//Created FXMLLoader in order to load controller as an object
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotoSearch.fxml"));
			
			//Load the file into root
			Parent root = loader.load();
			
			//Get the controller as an object
			PhotoSearchController controller = loader.<PhotoSearchController>getController();
			
			controller.setUser(user);
			
			
			Stage stage = new Stage();
			stage.setTitle("User Albums");
			stage.setScene(new Scene(root));
			stage.show();
			photoListView.getScene().getWindow().hide();
			
		}catch(Exception e) {
			
		}
	}
}
