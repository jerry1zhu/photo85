package controller;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import model.Album;
import model.Photo;
import model.User;
/**
 * Controller for copying and moving photos window.
 * @author Jerry Zhu
 * @author Chris Hartley
 */
public class CopyPhotoController {

	public User user;
	public Photo copy;
	public Album source;
	
	@FXML ListView<Album> AlbumList = new ListView<Album>();
	/**
	 * Sets the user object in this controller from the previous controller
	 * @param user
	 */
	public void setUser(User user) {
		this.user = user;
	}
	/**
	 * sets the album object in this controller from the previous controller
	 * @param album
	 */
	public void setAlbum(Album album) {
		source = album;
	}
	/**
	 * Sets the photo object in this controller from the previous controller
	 * @param photo
	 */
	public void setPhoto(Photo photo) {
		copy = photo;
	}
	/**
	 * Reloads the AlbumList ObservableList by clearing it and then adding an updated list of albums to it.
	 */
	public void loadAlbums() {
		
		//Changes only register if # of items changes so empty album list
		AlbumList.setItems(FXCollections.emptyObservableList());
		
		//...and then repopulate it to reflect changes
		AlbumList.setItems(FXCollections.observableList(user.albums));
		
		//select first item
		if(user.albums.size() > 0) AlbumList.getSelectionModel().selectFirst();
		
	}
	
	/**
	 * Attempts to add photos to another album.
	 * @return Boolean for if the process was successful or not
	 */
	public Boolean addToTarget() {
		int i = AlbumList.getSelectionModel().getSelectedIndex();
		
		if(i == -1) return false;
		
		Album target = AlbumList.getItems().get(i);
		
		if(source.name == target.name) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Copy Error");
			alert.setContentText("Please select an album that is not the source");
			alert.showAndWait();
			return false;
		}
		
		for(Photo p : target.photos) {
			if (p.path.equals(copy.path)){
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Copy Error");
				alert.setContentText("The photo you are trying to copy is already in the target album");
				alert.showAndWait();
				return false;
			}
		}
		
		target.addPhoto(copy);
		return true;
	}
	/**
	 * Attempts to copy photo to target album. If successful, return to AlbumView
	 */
	public void copy() {
		if(addToTarget()) {
			user.saveSelf();
			goHome();
		}
	}
	/**
	 * Attempts to move photo to target album. If successful, return to AlbumView
	 */
	public void move() {
		if(addToTarget()) {
			source.deletePhoto(copy);
			user.saveSelf();
			goHome();
		}
	}
	/**
	 * Set AlbumView as current scene
	 */
	public void goHome() {
		try {
			
			//Created FXMLLoader in order to load controller as an object
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AlbumHome.fxml"));
			
			//Load the file into root
			Parent root = loader.load();
			
			//Get the controller as an object
			PhotoController controller = loader.<PhotoController>getController();
			
			//Call setUser() on UserHomeController
			controller.setUser(user);
			controller.setAlbum(source);
			controller.loadPhotos();
			
			Stage stage = new Stage();
			stage.setTitle("Photos");
			stage.setScene(new Scene(root));
			stage.show();
			AlbumList.getScene().getWindow().hide();
			
		}catch(Exception e) {
			
		}
	}
	
	
}
