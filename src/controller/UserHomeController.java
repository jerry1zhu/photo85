package controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.Album;
import model.User;
/**
 * Controller for the user home page
 * @author Jerry Zhu
 * @author Chris Hartley
 */
public class UserHomeController{

	
	public User user;
	public static ObservableList<String> users = FXCollections.observableList(new ArrayList<>());
	
	@FXML ListView<Album> albumList = new ListView<Album>();

	
	/**
	 * Set User object from previous stage
	 * @param user
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	
	/**
	 * Set List of usernames from previous stage
	 * @param list	 
	 */
	public void setUserList(ObservableList<String> list) {
		users = list;
		users.remove(user.username);
	}

	/**
	 * Loads albums from this.user into albumList
	 */
	public void loadAlbums() {
		
		//Changes only register if # of items changes so empty album list
		albumList.setItems(FXCollections.emptyObservableList());
		
		//...and then repopulate it to reflect changes
		albumList.setItems(FXCollections.observableList(user.albums));
		
		//select first item
		if(user.albums.size() > 0) albumList.getSelectionModel().selectFirst();
		
	}
	
	
	/**
	 * Sets CreateAlbum as the active scene
	 */
	public void createAlbum() {
		Parent root;
		try {
			
			//Created FXMLLoader in order to load controller as an object
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/CreateAlbum.fxml"));
			
			//Load the file into root
			root = loader.load();
			
			//Get the controller as an object
			CreateAlbumController controller = loader.<CreateAlbumController>getController();
			
			//Call setUser() on UserHomeController
			controller.setOwner(user);
			
			Stage stage = new Stage();
			stage.setTitle("User Albums");
			stage.setScene(new Scene(root));
			stage.show();
			albumList.getScene().getWindow().hide();
			
		} catch(Exception e) {
			
		}
		
	}
	
	/**
	 * Sets AlbumHome as the active scene
	 */
	public void openAlbum() {
		Parent root;
		int i;
		try {
		
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/AlbumHome.fxml"));
		
			root = loader.load();
			
			PhotoController controller = loader.<PhotoController>getController();
			controller.setUser(user);
			i = albumList.getSelectionModel().getSelectedIndex();
			if(i != -1) {
				controller.setAlbum(user.albums.get(i));
				controller.loadPhotos();
				Stage stage = new Stage();
				stage.setTitle("Photos");
				stage.setScene(new Scene(root));
				stage.show();
				albumList.getScene().getWindow().hide();
			}
			
			
		} catch(Exception e) {
			
		}
	}
	
	/**
	 * Sets SharedAlbums as the active scene
	 */
	public void openSharedAlbums() {
		Parent root;
		int i;
		try {
		
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/SharedAlbums.fxml"));
		
			root = loader.load();
			
			SharedAlbumsController controller = loader.<SharedAlbumsController>getController();
			controller.setUser(user);
			controller.loadAlbums();
	
			Stage stage = new Stage();
			stage.setTitle("SharedAlbums");
			stage.setScene(new Scene(root));
			stage.show();
			albumList.getScene().getWindow().hide();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Confirms user wants to delete album and removes it from thier albums
	 * @param event
	 */
	public void deleteAlbum(ActionEvent event){
		int i = albumList.getSelectionModel().getSelectedIndex();
		if(albumList.getSelectionModel().getSelectedIndex() == -1) return;
			
		Parent root;
		try {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Delete Album");
			String s = "Are you sure you want to delete album: "+user.albums.get(i).name +"?";
			alert.setContentText(s);
			ButtonType yes = new ButtonType("Yes");
	        ButtonType no = new ButtonType("No");	        
	        alert.getButtonTypes().clear();
	        alert.getButtonTypes().addAll(yes, no);
			Optional<ButtonType> result = alert.showAndWait();
			 
			if ((result.isPresent()) && (result.get() == yes)) {
				user.deleteAlbum(i);
				loadAlbums();
			}
			
		} catch(Exception e) {
			
		}
		
	}
	
	/**
	 * Opens dialgue to rename the selected album
	 * @param event
	 */
	public void renameAlbum(ActionEvent event) {
		int i = albumList.getSelectionModel().getSelectedIndex();
		
		if(i == -1) {
			//No song selected alert
			return;
		}
		
		TextInputDialog dialog = new TextInputDialog("");
		dialog.setTitle("Rename Album");
		dialog.setHeaderText("Renaming Album: " + user.albums.get(i).name);
		dialog.setContentText("Please Enter New Album Name:");

		// Traditional way to get the response value.
		Optional<String> result = dialog.showAndWait();
		
		if (result.isPresent() && !result.get().isEmpty()){
		   user.renameAlbum(i, result.get());
		   loadAlbums();
		   albumList.getSelectionModel().clearAndSelect(i);
		}
	}
	
	
	/**
	 * Opens dialgue to rename the selected album
	 * @param event
	 */
	public void openShareForm(ActionEvent event) {
		int i = albumList.getSelectionModel().getSelectedIndex();
		if(i == -1) {
			//No album selected alert
			return;
		}
		Parent root;
		try {
			
			//Created FXMLLoader in order to load controller as an object
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/ShareAlbumForm.fxml"));
			
			//Load the file into root
			root = loader.load();
			
			//Get the controller as an object
			ShareFormController controller = loader.<ShareFormController>getController();
			
			//Call setUser() on UserHomeController
			controller.setUser(user);
			controller.setUserList(users);
			controller.setAlbum(user.albums.get(i));
			
			
			Stage stage = new Stage();
			stage.setTitle("Share Album Form");
			stage.setScene(new Scene(root,300,200));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * This will handle the logout button for the users
	 * @param event 
	 */
	public void logout(ActionEvent event) {
		Parent root;
		try {
			/*logout confirmation alert*/
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Logging out");
			String s = "Are you sure you want to logout?";
			alert.setContentText(s);
			ButtonType yes = new ButtonType("Yes");
	        ButtonType no = new ButtonType("No");	        
	        alert.getButtonTypes().clear();
	        alert.getButtonTypes().addAll(yes, no);
			Optional<ButtonType> result = alert.showAndWait();
			 
			if ((result.isPresent()) && (result.get() == yes)) {
				root = FXMLLoader.load(getClass().getResource("/view/login.fxml"));
				Stage stage = new Stage();
				stage.setTitle("Photo Album by Chris Hartley and Jerry Zhu");
				stage.setScene(new Scene(root,400,400));
				stage.show();
				albumList.getScene().getWindow().hide();
			}
			
		} catch(Exception e) {
			
		}
	}
	
	
	/**
	 * Sets PhotoSearch as the active scene
	 */
	public void searchPhotos() {
		try {
			
			//Created FXMLLoader in order to load controller as an object
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/PhotoSearch.fxml"));
			
			//Load the file into root
			Parent root = loader.load();
			
			//Get the controller as an object
			PhotoSearchController controller = loader.<PhotoSearchController>getController();
			
			controller.setUser(user);
			
			
			Stage stage = new Stage();
			stage.setTitle("User Albums");
			stage.setScene(new Scene(root));
			stage.show();
			albumList.getScene().getWindow().hide();
			
		}catch(Exception e) {
			
		}
	}
}
