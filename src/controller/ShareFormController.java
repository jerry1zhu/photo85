package controller;

import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Alert.AlertType;
import model.Album;
import model.User;
/**
 * Controls form for sharing albums
 * @author Jerry Zhu
 * @author Chris Hartley
 */
public class ShareFormController {
	
	public User user;
	public Album album;
	public static ObservableList<String> users = FXCollections.observableList(new ArrayList<>());
	@FXML ChoiceBox userList;
	
	
	/**
	 * Set User object from previous stage
	 * @param user
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	/**
	 * Sets Album object from previous stage
	 * @param album
	 */
	public void setAlbum(Album album) {
		this.album = album;
	}
	
	
	/**
	 * Sets the choices of userList to the usernames of users on this system
	 * @param list
	 */
	public void setUserList(ObservableList<String> list) {
		users = list;
		userList.setItems(users);
		
		if(users.size()>0) userList.getSelectionModel().clearAndSelect(0);
	}
	
	
	/**
	 * Shares the currently selected album with the selected user from userList
	 */
	public void shareAlbum() {
		if(user.shareAlbum(album, userList.getValue().toString())) {
			userList.getScene().getWindow().hide();
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Shared Album");
			alert.setContentText("Album successfully shared with \"" +userList.getValue().toString()
					+"\". \nYou can both view the album in your \"Shared Albums\" but only you can make"
					+ " changes to the album.");
			alert.showAndWait();
		}
	}
}
